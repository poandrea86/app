<?php
require __DIR__ . '/vendor/autoload.php';
use App\Providers\Bootstrap;
use App\Providers\Display;

$app = new Bootstrap();
$container = $app->start();
$customerService = $container->get('CustomerService');

try {
    $customerId = (isset($argv[1])) ? $argv[1] : null;
    if(!isset($customerId)) {
        throw new Exception("Customer ID is required!");
    }
    Display::render($transactions = $customerService->getTransactions($customerId));
} catch (Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}
