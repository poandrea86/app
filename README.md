APP PROJECT
===============================

## Requirements
    docker
    docker-compose
    
## Configuration

### step 1
run command 
```bash
   docker-compose up -d
```
   
### step 2
run command 
```bash
    docker exec -it app bash
```
    
### step 3
Into docker container run
```bash
composer install
```
    
Now you are ready for run report & tests

## Get report
run command
```bash
docker exec -it app bash
```
Into docker container run
```bash
php index.php {customerId}
```

## Tests
run command
```bash
docker exec -it app bash
```
move to tests/ folder and run command
```bash
./vendor/bin/phpunit tests
```



    
    