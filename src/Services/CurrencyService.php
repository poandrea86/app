<?php

namespace App\Services;

use App\Repositories\CurrencyRepository;

class CurrencyService
{
    private $currencyRepository;

    public function __construct(CurrencyRepository $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
    }

    public function getPrice($text)
    {
        if (isset($text)) {
            preg_match("/[+-]?([0-9]*[.])?[0-9]+/", $text, $matched);
            $number = $matched[0];
            $symbol = preg_replace("/" . $number . "/", "", $text);
            return $this->getCurrency($number, $symbol);
        }
        return [];
    }

    private function getCurrency($value, $symbol)
    {
        $currencies = $this->currencyRepository->getCurrencies();
        $decodedSymbol = $this->currencyRepository->decodeCurrency($symbol);
        $currency = $currencies[$decodedSymbol];
        if ($currency > 0) {
            return [
                'currency' => '€ ' . number_format($value / $currency, 2,',', '.'),
                'exchange' => $currency
            ];

        }
        return [];
    }

}