<?php

namespace App\Services;

use App\Repositories\CustomerRepository;

class CustomerService
{
    private $customerRepository;
    private $currencyService;

    public function __construct(CustomerRepository $customerRepository, CurrencyService $currencyService)
    {
        $this->customerRepository = $customerRepository;
        $this->currencyService = $currencyService;
    }

    public function getTransactions(int $customerId): array {
        $transactions = $this->customerRepository->getTransactions();
        if (count($transactions) > 0) {
            return array_values(collect($transactions)->map(function ($transaction) {
                $price = $this->currencyService->getPrice($transaction[2]);
                return [
                    'customer_id' => (int) $transaction[0],
                    'date' => $transaction[1],
                    'original' => $transaction[2],
                    'exchange' => $price['exchange'],
                    'value' => $price['currency'],
                ];
            })->filter(function($transaction) use($customerId) {
                return $transaction['customer_id'] === $customerId;
            })->toArray());
        }
        return [];
    }
}