<?php

namespace App\Providers;

use jc21\CliTable;
use PHPUnit\Exception;

class Display
{
    public static function render($transactions) {
        try {
            if (count($transactions) > 0) {
                $table = new CliTable;
                $table->setTableColor('blue');
                $table->setHeaderColor('cyan');
                $table->addField('Date', 'date', false, 'white');
                $table->addField('Original', 'original', false, 'green');
                $table->addField('Exchange', 'exchange', false, 'green');
                $table->addField('EUR Value', 'value', false, 'green');
                $table->injectData($transactions);
                $table->display();
            } else {
                throw new \Exception("Transactions not found");
            }
        } catch (\Exception $e) {
            error_log($e->getFile() . " > " .$e->getMessage() . " at line " . $e->getLine() );
            return null;
        }
    }
}