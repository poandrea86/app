<?php

namespace App\Providers;

use DI\ContainerBuilder;
use function DI\autowire;

class Bootstrap
{
    public function start() {
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->addDefinitions($this->getDynamicClass());
        return  $containerBuilder->build();
    }

    private function getDynamicClass() {
        $dynamicClass = [];
        $files = glob(__DIR__ . "/../{*/}*.php", GLOB_BRACE);
        foreach ($files as $file) {
            $name = preg_replace("/\.php/", "", basename($file));
            $ns = null;
            $handle = fopen($file, "r");
            if ($handle) {
                while (($line = fgets($handle)) !== false) {
                    if (strpos($line, 'namespace') === 0) {
                        $parts = explode(' ', $line);
                        $ns = rtrim(trim($parts[1]), ';');
                        break;
                    }
                }
                fclose($handle);
            }
            $dynamicClass[$name] = autowire("\\" . $ns . "\\" . $name);
        }
        return $dynamicClass;
    }
}