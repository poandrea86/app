<?php

namespace App\Repositories;

class CustomerRepository
{
    private $path;
    public function __construct()
    {
        $this->path = __DIR__ . '/../../storage';
    }

    public function getTransactions(): array {
        return $this->loadData();
    }

    private function loadData(): array {
        $data = [];
        $handle = fopen($this->path . '/data.csv', 'r');
        if ($handle !== false) {
            while (($row = fgetcsv($handle, 1000, ";", '"')) !== false) {
                $data[] = $row;
            }
            fclose($handle);
            return array_slice($data, 1);
        }
        return [];
    }
}