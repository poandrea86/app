<?php


namespace App\Repositories;


class CurrencyRepository
{
    public function getCurrencies()
    {
        return [
            'EUR' => 1,
            'USD' => 1.18,
            'GPB' => 0.90
        ];
    }

    public function decodeCurrency($convertCurrency)
    {
        $currency = [
            '€' => 'EUR',
            '$' => 'USD',
            '£' => 'GPB'
        ];
        return $currency[$convertCurrency];
    }
}