FROM php:7.3.17-apache

RUN apt-get update \
 && apt-get install -y git zlib1g-dev vim libzip-dev \
 && a2enmod rewrite \
 && curl -sS https://getcomposer.org/installer \
  | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /var/www/html