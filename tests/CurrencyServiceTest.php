<?php declare(strict_types=1);

use App\Repositories\CurrencyRepository;
use App\Services\CurrencyService;
use PHPUnit\Framework\TestCase;

final class CurrencyServiceTest extends TestCase
{
    public function testGetExchange()
    {
        $currencyRepository = new CurrencyRepository();
        $currencyService = new CurrencyService($currencyRepository);
        $price = $currencyService->getPrice(null);

        $this->assertArrayHasKey('exchange', $price);
    }

    public function testGetCurrency()
    {
        $currencyRepository = new CurrencyRepository();
        $currencyService = new CurrencyService($currencyRepository);
        $price = $currencyService->getPrice('€45.00');

        $this->assertArrayHasKey('exchange', $price);
    }

}



