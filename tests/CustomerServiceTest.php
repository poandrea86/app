<?php declare(strict_types=1);

use App\Providers\Display;
use App\Repositories\CurrencyRepository;
use App\Repositories\CustomerRepository;
use App\Services\CurrencyService;
use App\Services\CustomerService;
use PHPUnit\Framework\TestCase;

final class CustomerServiceTest extends TestCase
{
    public function testCanLoadCustomerTransaction()
    {
        $customerRepository = new CustomerRepository();
        $currencyRepository = new CurrencyRepository();
        $currencyService = new CurrencyService($currencyRepository);
        $customerService = new CustomerService($customerRepository, $currencyService);
        $transactions = $customerService->getTransactions(1);

        $this->assertIsArray($transactions);
        $this->assertGreaterThan(0, count($transactions));
    }

    public function testCanDisplayRender() {
        $customerRepository = new CustomerRepository();
        $currencyRepository = new CurrencyRepository();
        $currencyService = new CurrencyService($currencyRepository);
        $customerService = new CustomerService($customerRepository, $currencyService);
        $transactions = $customerService->getTransactions(4);
        $this->assertEmpty(Display::render($transactions));
    }

    public function testFileExist() {
        $this->assertFileExists(__DIR__ . '/../storage/data.csv');
    }

}



